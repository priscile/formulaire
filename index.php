<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>formulaire</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css%22%3E">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css%22%3E">
    <link rel="stylesheet" href="css/form.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  
</head>
<body>
 <div class="container-fluid">
   <div class="row col-md-12">
     <div class=" formulaire col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-4 col-xl-6" style="margin-top:20px; padding-bottom:20px; background-color:#8492A0; ">
       <form enctype="multipart/form-data" action="traitement.php" method="post" id="myform">
        <h4 class=inscription class=form-title>INSCRIPTION</h4>
        <label>nom </label>
          <div class="input-group">
              <span class="input-group-addon"style="color:black;;" >
                <span class="glyphicon glyphicon-user"></span>
              </span>
              <input class="form-control" type="text" name="nom" id="nom" minlength="4" maxlength="50" required>
          </div>
              
        <label for="">prenom</label>
              <div class="input-group ">
                  <span class="input-group-addon"style="color:black;" >
                    <span class="glyphicon glyphicon-user "></span>
                  </span>
                  <input class="form-control" type="text" name="prenom" id="prenom" minlength="4"  maxlength="50" required> <br> 
              </div>
        <label for="">sexe </label>
              <div class="form-control">
                  <label for="">Masculin</label>
                   <input type="radio" name="sexe"  value="Homme" required>
                  <label for="">Feminin</label>
                    <input type="radio" name="sexe" value="Femme"  required>
              </div> <br>
        <label for="">date naissance</label>
            <input class="form-control" type="date" name="date_naissance" id=""> <br>
              <label for="">numero de telephone</label>
              <div class="input-group">
                  <span class="input-group-addon"style="color:black;;" >
                    <span class="glyphicon glyphicon-phone"></span>
                  </span>
                  <input class="form-control" type="number" name="numero_tel"  required>
              </div>
       <label for="">email</label>
              <div class="input-group">
                  <span class="input-group-addon"style="color:black;" >
                    <span class="glyphicon glyphicon-envelope"></span>
                  </span>
                  <input class="form-control" type="email" name="email" required>
             </div>
        <label for="">mot de passe</label>
              <div class="input-group">
                 <span class="input-group-addon"style="color:black;" >
                      <span class="glyphicon glyphicon-lock"></span>
                  </span>
                  <input class="form-control" type="password"  name="passeword" required> 
              </div><br>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"style="color:black;" >
                              <span class="glyphicon glyphicon-globe"></span>
                        </span>
                  
                          <select name="choix_pays" class=" form-control" required="">
                                <option selected="selected" disabled="">Choix du Pays...</option>
                                  <option value="Belize"> Belize <span>+501</span></option>
                                  <option value="Benin"> Benin <span>+229</span></option>
                                  <option value="Bhutan"> Bhutan <span>+975</span></option>
                                  <option value="Bolivia"> Bolivia <span>+591</span></option>
                                  <option value="Bonaire"> Bonaire, Sint Eustatius <span>+599</span></option>
                                  <option value="Bosnia"> Bosnia and Herzegovina <span>+387</span></option>
                                  <option value="Botswana"> Botswana <span>+267</span></option>
                                  <option value="Brazil"> Brazil <span>+55</span></option>
                                  <option value="Brunei"> Brunei <span>+673</span></option>
                                  <option value="Bulgaria"> Bulgaria <span>+359</span></option>
                                  <option value="cambodia"> Cambodia <span>+855</span></option>
                                  <option value="cameroun"> Cameroon <span>+237</span></option>
                                  <option value="canada"> Canada <span>+1</span></option>
                                  <option value="cape"> Cape Verde <span>+238</span>
                                  <option value="cayman"> Cayman Islands <span>+1345</span>
                                  <option value="rca"> Central African Republic <span>+236</span></option>
                                  <option value="chad"> Chad <span>+235</span></option>
                                  <option value="chile"> Chile <span>+56</span></option>
                                  <option value="china"> China <span>+86</span></option>
                                  <option value="colombia"> Colombia <span>+57</span></option>
                                  <option value="comoros"> Comoros and Mayotte <span>+269</span></option>
                                  <option value="congo"> Congo <span>+242</span></option>
                                  <option value="rdc"> Congo Dem Rep <span>+243</span></option>
                                  <option value="cook islands"> Cook Islands <span>+682</span></option>
                                  <option value="costa rica"> Costa Rica <span>+506</span></option>
                                  <option value="cote d'ivoire"> Cote d'Ivoire <span>+225</span></option>
                                  <option value="croatia"> Croatia <span>+385</span></option>
                                  <option value="cuba"> Cuba <span>+53</span></option>
                                  <option value="curacao"> Curaçao <span>+599</span></option>
                                  <option value="cyprus"> Cyprus <span>+357</span></option>
                                  <option value="czech"> Czech Republic <span>+420</span></option>
                                  <option value="denmark"> Denmark <span>+45</span></option>
                                  <option value="diego gracia"> Diego Garcia <span>+246</span></option>
                                  <option value="Djibouti"> Djibouti <span>+253</span></option>
                                  <option value=" Ecuador"> Ecuador <span>+593</span></option>
                                  <option value="Egypt"> Egypt <span>+20</span></option>
                                  <option value="El Salvador"> El Salvador <span>+503</span></option>
                                  <option value="Equatorial Guinea"> Equatorial Guinea <span>+240</span></option>
                                  <option value="Eritrea"> Eritrea <span>+291</span></option>
                                  <option value="Estonia"> Estonia <span>+372</span></option>
                                  <option value="Ethiopia"> Ethiopia <span>+251</span></option>
                                  <option value="Falkland Islands"> Falkland Islands <span>+500</span></option>
                                  <option value="Faroe Islands"> Faroe Islands <span>+298</span></option>
                                  <option value="Fiji"> Fiji <span>+679</span></option>
                                  <option value="Finland"> Finland <span>+358</span></option>
                                  <option value="France"> France <span>+33</span></option>
                                  <option value="French Guiana"> French Guiana <span>+594</span></option>
                                  <option value="French Polynesia"> French Polynesia <span>+689</span></option>
                                  <option value="Gabon"> Gabon <span>+241</span></option>
                                  <option value="Gambia"> Gambia <span>+220</span></option>
                                  <option value="Georgia"> Georgia <span>+995</span></option>
                                  <option value="Kuwait"> Kuwait<span>+965</span></option>
                                  <option value="Kyrgyzstan"> Kyrgyzstan<span>+996</span></option>
                                  <option value="Laos"> Laos<span>+856</span></option>
                                  <option value="Latvia"> Latvia<span>+371</span></option>
                                  <option value="Lebanon"> Lebanon<span>+961</span></option>
                                  <option value="Lesotho"> Lesotho<span>+266</span></option>
                                  <option value="Liberia"> Liberia<span>+231</span></option>
                                  <option value="Libya"> Libya<span>+218</span></option>
                                  
                          </select>
                    </div>
                </div>
                  <label for="">photo profil</label>
                  <div class="input-group">
                    <span class="input-group-addon"style="color:black;" >
                          <span class="glyphicon glyphicon-picture"></span>
                    </span> 
                    <input type="file" name="photo" class="form-control" accept="image/*"onchange="loadFile(event)" required> 
                  </div><br>
                  <div class="col-md-offset-4 col-md-4"style="margin-top: 10px; margin-bottom: 10px; height: 120px; width: 120px; border-radius: 50%;">
                              <img id="pp" style="height: 120px; width: 120px; border-radius: 50%;" /> 
                  </div>
              <input type="submit" class="btn btn-block btn-primary" value=envoyer name="envoyer">

        </form>
     </div>
  </div>
 </div>
<script type="text/javascript" src="javascript/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
    var loadFile = function(event) {
        var profil = document.getElementById('pp');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
      nom.addEventListener('focus', function(){	// on se rassure que les erreurs ne s'affichent pas lorsqu'on est dans le champ.
		alertnom.style.opacity='0';
	});

	nom.addEventListener('blur', testnom);  // on controle la valeur laissee dans le champ
	function testnom(){
		// console.log(nom.value);  //verification du fonctionnement
	 	if(nom.value.length<3){  //test de taille de contenu
	 		alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
	 		alertnom.style.opacity='1';   //penser a le faire avec une duree definie. 
			return false;
		}
		else if(contient_specialchart(nom.value)){  // test de caracteres non indiques
			alertnom.innerHTML = "Le nom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
			alertnom.style.opacity = '1';
			return false;
		}else{
			return true;
		}
	}
	function contient_specialchart(char){
		var special = ['@','!','~','<','>','?',' " '," ' ",'*','(',')','^','%','$','#','&','{','}','[',']',';',];

		for (var i = special.length - 1; i >= 0; i--) { //pour chaque element de special
			for (var id in char) {  // on se rassure qu'il ne soit pas dans char
				if(char[id]==special[i]){
					return true;
				}
			}
		}
		return false;
	}

	// traitement du prenom ************************************************************************
	var	prenom = document.getElementById('prenom'),
		alertprenom = document.getElementById('alertprenom');

	prenom.addEventListener('focus', function(){	// on se rassure que les erreurs ne s'affichent pas lorsqu'on est dans le champ.
		alertprenom.style.opacity='0';
	});

	prenom.addEventListener('blur', testprenom);  // on controle la valeur laissee dans le champ
	function testprenom(){
		// console.log(nom.value);  //verification du fonctionnement
	 	if(prenom.value.length<3){  //test de taille de contenu
	 		alertprenom.innerHTML = "Le prenom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
	 		alertprenom.style.opacity='1';   //penser a le faire avec une duree definie. 
			return false;
		}
		else if(contient_specialchart(prenom.value)){  // test de caracteres non indiques
			alertprenom.innerHTML = "Le prenom doit avoir au minimum 3 lettres sans les caracteres speciaux tels que @ ou % ...";
			alertprenom.style.opacity = '1';
			return false;
		}else{
			return true;
		}
	}

	// traitement du phone number ***********************************************************************
	var phone = document.getElementById('phone'),
		alertphone = document.getElementById('alertphone');

	phone.addEventListener('focus', function(){	// on se rassure que les erreurs ne s'affichent pas lorsqu'on est dans le champ.
		alertphone.style.opacity='0';
	});
		
	phone.addEventListener('blur', testphone);
	function testphone(){
		console.log(typeof(phone.value%1));
		console.log(phone.value%1);
		if (phone.value%1!=0){  // on se rassure que le champ ne contient que des chiffres.
			console.log('ici la fonction'); 
			alertphone.innerHTML = "Le numero de telephone ne doit contenir que des chiffres sans espace ni caracteres tels que @ ou % ...";
			alertphone.style.opacity = '1';
			return false;
		}else{
			return true;
		}
		
	}

	// traitement de l'adresse mail *****************************************************************
	var mail = document.getElementById('email');
		alertmail = document.getElementById('alertmail');

	mail.addEventListener('focus', function(){	// on se rassure que les erreurs ne s'affichent pas lorsqu'on est dans le champ.
		alertmail.style.opacity='0';
	});

	mail.addEventListener('blur', testmail);
	function testmail(){
		var mail_indic='0';
		var mail_format=[],mail_ending=[];
		mail_format=['gmail.com','yahoo.com','yahoo.fr','hotmail.com']; // les formats de mail acceptes

		for(var i=0; i<mail.value.length; i++){  // onparcours l'adresse mail saisie pour retrouver le caractere @
			if(mail.value[i]=='@'){		// si @ est reouve on le garde comme indicateur
				mail_indic=mail.value[i];
			}
		}
		
		if(mail_indic=='@'){         
			mail_ending=mail.value.split('@');   //si on a notre indicateur on l'utilise pour separer l'adresse entree en 2 blocks
			
			for (var i = 0; i < mail_format.length; i++) {  // on parcours le tableau des formats de mail.
				console.log(mail_format[i]);
				console.log(mail_format.length);
				if(mail_format[i]==mail_ending[1]){		// on verifie si la partie entree en adresse apres le @ vaut bien l'une des valeurs souhaitee
					return true;
				}
				else if(i==mail_format.length-1 && mail_format[i]!=mail_ending[1]){   // si jusqu'au dernier element des formats on ne trouve rien alors erreur
					alertmail.innerHTML = "Le format de mail attendu est 'exemple@xmail.xxx";
					alertmail.style.opacity='1';
					return false;
				}
			}
		}
		else{ 		// si on n'a pas l'indicateur, l'adresse mail n'est pas valide
			alertmail.innerHTML = "Le format de mail attendu est 'exemple@xmail.xxx";
			alertmail.style.opacity = '1';
			return false;
		}
  }
  </script>
</body>
</html>